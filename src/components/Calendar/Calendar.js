import React, { useEffect, useState } from 'react';
import * as daysData from '../../data/calendar.json'
import ReservationForm from '../ReservationForm/ReservationForm';
import './Calendar.css';

const Calendar = () => {
  const [days, setDays] = useState(daysData.default);
  const [reservations, setReservations] = useState([]);
  const [clicked, setClicked] = useState(false);
  const [reservationId, setReservationId] = useState(1);
  const statusColor = {
    blocked: 'LightPink',
    bookable: 'SpringGreen',
    booked: 'Khaki'
  }

  useEffect(() => {
  }, [clicked]);

  const reserve = (guest, from, to) => {
    const blockedDays = days.filter(el => el.date >= from && el.date <= to && (el.status === 'booked' || el.status === 'blocked'));
    const firstCalendarDay = days[0];
    const lastCalendarDay = days[days.length - 1];

    if (from < firstCalendarDay.date || from > lastCalendarDay.date || to < firstCalendarDay.date || to > lastCalendarDay.date) {
      return alert('The selected dates are not available for our booking season.');
    }

    if (blockedDays.length === 0) {
      const selectedDays = days.map(day => {
        if (day.date >= from && day.date <= to) {
          day.status = 'booked';
          setReservations([...reservations, {
            id: reservationId,
            startDate: from,
            endDate: to,
            guest: guest,
          }]);
          setReservationId(reservationId + 1);
        }

        return day;
      });

      setDays(selectedDays);
      setClicked(false);

      return;
    }
    const warning = blockedDays.map(day => day.date).join(', ');

    alert(`The following dates are unavailable: ${warning}`);
  }

  const getReservation = (date) => reservations.find(el => el.startDate <= date && date <= el.endDate)

  return (
    <>
      <h1>FantasticStay Calendar</h1>
      <div className='days'>
        {
          days.map(el => {

            return (
              <div className='day'>
                <div className='date'>{el.date}</div>
                <div className='status' style={{ backgroundColor: statusColor[el.status] }}>{el.status}</div>
                <div className='price'><b>{el.price}</b>BGN</div>
                {getReservation(el.date)?.id ? <div className='reservation-id'>ID: {getReservation(el.date)?.id}</div> : ''}
                {getReservation(el.date)?.guest ? <div className='guest'>Guest: {getReservation(el.date)?.guest}</div> : ''}
              </div>
            )
          })
        }
      </div>

      <div className='reserve'>
        <button className='res-btn' onClick={() => clicked ? setClicked(false) : setClicked(true)}>
          {clicked ? 'Cancel' : 'Create a reservation'}
        </button>

        {clicked ? <ReservationForm days={days} reserve={reserve} /> : ""}
      </div>
      <br />
    </>
  )

}

export default Calendar;