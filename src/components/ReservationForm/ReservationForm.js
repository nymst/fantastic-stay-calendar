import { useState } from 'react';
import './ReservationForm.css'

const ReservationForm = ({ reserve }) => {
  const [guest, setGuest] = useState('');
  const [startingDate, setStartingDate] = useState('');
  const [endDate, setEndDate] = useState('');

  return (
    <div className="reservation">

      <div className='guest'>
        <p>Input guest name below:</p>
        <input value={guest} onChange={e => setGuest(e.target.value)} />
      </div>
      <br />

      <div className='starting-date'>
        <p>Select Check-in date</p>
        <input type="date" value={startingDate} onChange={e => setStartingDate(e.target.value)} />
      </div>
      <br />

      <div className='end-date'>
        <p>Select Check-out date</p>
        <input type="date" value={endDate} onChange={e => setEndDate(e.target.value)} />
      </div>
      <br />

      <div className='submit'>
        <button className='submit-btn' onClick={() => reserve(guest, startingDate, endDate)}>Submit</button>
      </div>

    </div>
  )
}

export default ReservationForm;